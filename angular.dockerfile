FROM node:latest

WORKDIR /var/www

RUN git clone https://gitlab.com/alexeflying/revolvairwebclient.git

WORKDIR /root

RUN npm install -g @angular/cli@1.6.8

WORKDIR /var/www/revolvairwebclient

RUN npm install

EXPOSE 4200

CMD ["ng", "serve"]
