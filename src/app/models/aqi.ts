export class Aqi {
  aqi: number;
  average: number;
  color: string;
  label: string;
  maximum: number;
}